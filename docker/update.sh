docker-compose build
docker-compose up -d

docker-compose exec -u "$(id -u)" pipeline-web composer install
docker-compose exec -u "$(id -u)" pipeline-web php artisan migrate
docker-compose exec tms-api-solr solr create -c testCore
