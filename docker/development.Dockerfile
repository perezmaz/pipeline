FROM php:8.1-apache

WORKDIR /var/www/html

# Install Composer from latest Docker image
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

# Section to install PHP
RUN apt-get update -y && apt-get install -y \
    # PHP Modules
        curl libicu-dev libxml2-dev libzip-dev zip unzip zlib1g-dev \
    # Additional applications
        git

    # Support internationalization
RUN docker-php-ext-configure intl \
    && docker-php-ext-install intl

RUN docker-php-ext-install pdo pdo_mysql \
    && docker-php-ext-enable pdo_mysql

    # Support XML data format
RUN docker-php-ext-install xml

    # Support (un-)packing files using/via PHP
RUN docker-php-ext-install zip

    # Add Xdebug
RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

    # Add redis
RUN pecl install redis \
    && docker-php-ext-enable redis

## Copy PHP configurations
COPY application/php/php.ini application/php/php-development.ini application/php/xdebug.ini /usr/local/etc/php/conf.d/

# Section to configure Apache 2 web server

    # Ensure log directory exists
RUN mkdir /var/www/log
    # Ensure directory for SSL certificate handling exists
RUN mkdir /etc/apache2/ssl

RUN openssl req -new -newkey rsa:4096 -days 3650 -nodes -x509 -subj \
#RUN openssl req -newkey rsa:4096 -days 3650 -nodes -x509 -subj \
   "/C=NL/ST=Noord Brabant/L=Veghel/O=BAS Trucks/CN=localhost" \
   -keyout /etc/apache2/ssl/ssl.key -out /etc/apache2/ssl/ssl.crt

    # Configure SSL setup / headers
COPY application/apache/ssl-params.conf /etc/apache2/conf-available/
RUN a2enconf ssl-params

    # Enable modules
RUN a2enmod env headers deflate expires rewrite ssl
RUN a2enmod ssl rewrite

    # Copy and enable site config
COPY application/apache/100-site.conf /etc/apache2/sites-available/
RUN a2ensite 100-site
RUN a2dissite 000-default

# create local user inside docker and make apache use this user to solve permissions
ARG APACHE_USER=www-data
ARG APACHE_GROUP=www-data
ARG USER_ID=1000

# Set env vars used by apache
ENV APACHE_RUN_GROUP=${APACHE_GROUP}
ENV APACHE_RUN_USER=${APACHE_USER}

RUN id -u $APACHE_USER || useradd -rm -s /bin/sh -g ${APACHE_GROUP} -u ${USER_ID} ${APACHE_USER}
